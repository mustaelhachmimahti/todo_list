# TODO LIST

## Commence

1. En gros, vous devez créer une base de données
et importez la table que vous avez dans le repo.

2. Saisir les données dans le fichier de connexion

3. Ouvrez le serveur avec php -S 0.0.0.0.0:8000

4. Utiliser toutes les options, ajouter, supprimer et modifier


### Fonctionnement

1. Vous avez la page http://localhost:8000/src/routes/users.php où vous pouvez trouver tous les utilisateurs
2. Vous avez assui la page http://localhost:8000/src/routes/task.php où vous pouvez trouver toutes les tâches.
3. Enfin, vous avez la page http://localhost:8000/src/index.php où nous pouvons attribuer des tâches aux utilisateurs. Il y a aussi une `input
   select`  pour voir qui est assigné à quelle tâche et dans la même `input` nous pouvons supprimer les personnes assignées à une tâche.
4. On ne peut pas assigner deux fois le même utilisateur à la même tâche.