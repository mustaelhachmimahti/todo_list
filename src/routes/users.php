<?php
require './../connection/connection.php'
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" header
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./../style/userStyle.css">
    <title>Document</title>
</head>
<body>
<?php
if (isset($_POST['delete']) === true) {
    $id = $_GET['id_user'];
    $mysqli->query("DELETE FROM users WHERE id_user = $id");
    $mysqli->query("DELETE FROM user_tasks WHERE id_user = $id");
    header("Location: users.php");
}
if (isset($_POST['change_end']) === true) {
    $id = $_POST['id_url'];
    $first_name = $_POST['change_name'];
    $last_name = $_POST['change_last_name'];
    $rol = $_POST['change_rol'];
    $mysqli->query("UPDATE users SET first_name = '$first_name', last_name = '$last_name', rol = '$rol' WHERE id_user = $id;");
    header("Location: users.php");
}
if (isset($_POST['add_end']) === true) {
    $first_name = $_POST['add_first_name'];
    $last_name = $_POST['add_last_name'];
    $rol = $_POST['add_rol'];
    $mysqli->query("INSERT INTO users (id_user, first_name, last_name, rol) VALUES (NULL, '$first_name', '$last_name', '$rol')");
}
?>
<h2>USERS</h2>
<table>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Rol</th>
    </tr>
    <?php
    $result_task = $mysqli->query("SELECT * FROM users");
    foreach ($result_task as $result) {
        ?>
        <tr>
            <td><?php echo $result['first_name'] ?></td>
            <td><?php echo $result['last_name'] ?></td>
            <td><?php echo $result['rol'] ?></td>
            <form action="./users.php?id_user=<?php echo $result['id_user'] ?>" method="POST">
                <td><input type="submit" name="change" value="Change"></td>
                <td><input type="submit" name="delete" value="Delete"></td>
                <input type="hidden" name="first_name_user" value="<?php echo $result['first_name'] ?>">
                <input type="hidden" name="last_name_user" value="<?php echo $result['last_name'] ?>">
                <input type="hidden" name="rol_user" value="<?php echo $result['rol'] ?>">
            </form>
        </tr>
        <?php
    }
    ?>
</table>

<?php
if (isset($_POST['change']) === true) {
    ?>
    <form method="POST">
        <input type="hidden" value="<?php echo $_GET['id_user'] ?>" name="id_url">
        <p>Make your changes...</p>
        <input type="text" name="change_name" value="<?php echo $_POST['first_name_user'] ?>">
        <input type="text" name="change_last_name" value="<?php echo $_POST['last_name_user'] ?>">
        <input type="text" name="change_rol" value="<?php echo $_POST['rol_user'] ?>">
        <input type="submit" name="change_end" value="Change it">
    </form>
    <?php
}
?>
<h3>ADD TASK</h3>
<form method="POST">
    <p>Assign task to:</p><br>
    <input type="text" name="add_first_name" placeholder="First Name">
    <input type="text" name="add_last_name" placeholder="Last Name">
    <input type="text" name="add_rol" placeholder="Rol Name">
    <input type="submit" name="add_end" value="Add it">
</form>
</body>
</html>