<?php
require './../connection/connection.php'
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./../style/userStyle.css">
    <title>Document</title>
</head>
<body>
<?php
if (isset($_POST['delete']) === true) {
    $id = $_GET['id'];
    $mysqli->query("DELETE FROM task WHERE id = $id");
    $mysqli->query("DELETE FROM user_tasks WHERE id = $id");
    header("Location: task.php");
}
if (isset($_POST['change_end']) === true) {
    $id = $_POST['id_url'];
    $value_change = $_POST['change_task'];
    $mysqli->query("UPDATE task SET task_name = '$value_change' WHERE id = $id");
    header("Location: task.php");
}
if (isset($_POST['add_end']) === true) {
    $taskAdded = $_POST['task_add_name'];
    $mysqli->query("INSERT INTO task (task_name) VALUE   ('$taskAdded')");
}
?>
<h2>TASK</h2>
<table>
    <tr>
        <th>Task</th>
        <th>Date</th>
    </tr>
    <?php
    $result_task = $mysqli->query("SELECT * FROM task");
    foreach ($result_task as $result) {
        ?>
        <tr>
            <td><?php echo $result['task_name'] ?></td>
            <td><?php echo $result['date_time'] ?></td>
            <form action="./task.php?id=<?php echo $result['id'] ?>" method="POST">
                <input type="hidden" value="<?php echo $result['task_name'] ?>" name="content_task">
                <td><input type="submit" name="change" value="Change"></td>
                <td><input type="submit" name="delete" value="Delete"></td>
            </form>
        </tr>
        <?php
    }
    ?>
</table>

<?php
if (isset($_POST['change']) === true) {
    ?>
    <form method="POST">
        <input type="hidden" value="<?php echo $_GET['id'] ?>" name="id_url">
        <label for="change_task">Make your change...</label><br>
        <input type="text" name="change_task" id="change_task" value="<?php echo $_POST['content_task'] ?>">
        <input type="submit" name="change_end" value="Add it">
    </form>
    <?php
}
?>
<h3>ADD TASK</h3>
<form method="POST">
    <label for="addTaskName">What kind task do you want to add ?</label><br>
    <input type="text" name="task_add_name" id="addTaskName">
    <input type="submit" name="add_end" value="Add it">
</form>
</body>
</html>