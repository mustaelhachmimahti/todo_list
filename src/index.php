<?php
require './connection/connection.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./style/style.css">
</head>
<body>
<?php
$sqlSelTask = "SELECT * FROM task";
$rest = $mysqli->query($sqlSelTask);
?>
<div class="user-and-task">
    <div class="task">
        <form method="POST">
            <?php
            foreach ($rest as $result) {
                ?>
                <div>
                    <input type="radio" id="task" name="id_task" value="<?php echo $result['id'] ?>">
                    <label for="task"><?php echo $result['task_name'] ?></label>
                </div>
                <?php
            }
            ?>
    </div>
    <?php
    $sqlSelUser = "SELECT * FROM users";
    $resu = $mysqli->query($sqlSelUser);
    ?>
    <div class="user">
        <?php
        foreach ($resu as $result) {
            ?>
            <div>
                <input type="checkbox" id="user" name="id_user[]" value="<?php echo $result['id_user'] ?>">
                <label for="user"><?php echo $result['first_name'] . " " . $result['last_name'] ?></label>
            </div>
            <?php
        }
        ?>
        <input type="submit" name="assign" value="assign">
        </form>
    </div>
</div>
<?php
if (isset($_POST['assign'])) {
    $id_task = $_POST['id_task'];
    $id_user = $_POST['id_user'];
    if (!empty($id_user)) {
        foreach ($id_user as $user) {
            $mysqli->query("INSERT INTO user_tasks (id, id_user) VALUES ('$id_task', '$user')");
        }
    }
}
?>
<form method="POST" action="./index.php">
    <label for="fil">Choose a task, to see how is working on it...</label>

    <select id="fil" name="task_filter">
        <option selected disabled hidden>Choose task</option>
        <?php
        $sqlAllSel = "SELECT * FROM task";
        $respons = $mysqli->query($sqlAllSel);
        foreach ($respons as $res) {
            ?>
            <option value="<?php echo $res['id'] ?>"><?php echo $res['task_name'] ?></option>
            <?php
        }
        ?>
    </select>
    <input type="submit" name="chose_task" value="Filter">
</form>

<?php
if (isset($_POST['chose_task'])) {
    $filter_id = $_POST['task_filter'];
    $sqlReq = "SELECT * FROM task JOIN user_tasks ON user_tasks.id = task.id JOIN users ON users.id_user = user_tasks.id_user WHERE task.id = $filter_id";
    $resultado = $mysqli->query($sqlReq);
    ?>
    <form action="" method="post">
        <?php
        foreach ($resultado as $names) {
            ?>
            <div>
                <input type="checkbox" id="user" name="id_the_user[]" value="<?php echo $names['id_user'] ?>">
                <label for="user"><?php echo $names['first_name'] . " " . $names['last_name'] ?></label>
                <input type="hidden" name="id_task_assigned" value="<?php echo $names['id'] ?>">
            </div>
            <?php
        }
        ?>
        <input type="submit" value="Delete assign" name="del_ass">

    </form>
    <?php
}

if (isset($_POST['del_ass'])) {
    $idOfUser = $_POST['id_the_user'];
    $idOfTask = $_POST['id_task_assigned'];
    if (!empty($idOfUser)) {
        foreach ($idOfUser as $idUs) {
            $mysqli->query("DELETE FROM user_tasks WHERE id = $idOfTask AND id_user =  $idUs");
        }
    }
}

if (isset($_POST['delete_assign'])) {
    $id_of_task = $_GET['id'];
    $id_of_user = $_GET['id_user'];
    $mysqli->query("DELETE FROM user_tasks WHERE id = $id_of_task AND id_user = $id_of_user");
    header("Location: index.php");
}

?>
</body>
</html>